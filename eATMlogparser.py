# -*- coding: utf-8 -*-
"""
Created on Fri Mar 24 10:39:41 2023

@author: jopwo
"""

import json
import re
from pathlib import Path
from datetime import datetime

import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator
import numpy as np
import pandas as pd

import collections

# import generic_functions as generic
# from eATMstepper import frequency_doublecomb

class Log(collections.abc.MutableSequence):
    '''
    Sequence of ATM Experiments, presumably from a single ATM logfile.
    '''
    
    def __init__(self,
                 experiments: list):
        self.experiments = []
        self.experiments.extend(experiments)
            
    def check(self, v):
        if not isinstance(v, Experiment):
            raise TypeError("ERROR: Log class only takes Experiment instances!")
    
    def __delitem__(self, i): del self.experiments[i]
    
    def __getitem__(self, i): return self.experiments[i]
    
    def __len__(self): return len(self.experiments)
    
    def __setitem__(self, i, v):
        self.check(v)
        self.experiments[i] = v
    
    def insert(self, i, v):
        self.check(v)
        self.experiments.insert(i, v)
        
    @classmethod
    def from_ATMlogfile(cls, path_log: Path) -> list:
        '''
        Extracts datetime, tune/motor positions and SWR values from ATM log files.
        Treats 'Experiment Stop' as a boundary between list items.
        '''
        
        pat_exp_start = re.compile('New Experiment Started')
        pat_exp_stop = re.compile('Experiment Stop')
        pat_tuning_started = re.compile('Tuning Started')
        pat_tuning_finished = re.compile('Tuning Finished')
        pat_no_tuning_necessary = re.compile('No Tuning Necessary')
        
        pat_date = re.compile('^[0-9]{2}\\.[0-9]{2}\\.[0-9]{4}')
        pat_time = re.compile('[0-9]{2}:[0-9]{2}:[0-9]{2}')
        pat_fwrd = re.compile('(?<=FWD \\(dBm\\): )-?[0-9]+\\.[0-9]+(?=,)')
        pat_refl = re.compile('(?<=REF \\(dBm\\): )-?[0-9]+\\.[0-9]+(?=,)')
        pat_tune = re.compile('(?<=TunePosition: )[0-9]+(?=,)')
        pat_matc = re.compile('(?<=MatchPosition: )[0-9]+(?=,)')
        pat_swr  = re.compile('(?<=SWR: )[0-9]+\\.[0-9]+$')
        
        log_initialiser = []
        experiment_initialiser = []
        tuningevent_initialiser = {}
        with open(path_log, 'r') as src:
            for i,line in enumerate(src):
                if pat_exp_start.search(line): # experimented start
                    exp_unfinished = True
                    experiment_initialiser = []
                    
                elif pat_exp_stop.search(line): # experiment stop
                    exp_unfinished = False
                    log_initialiser.append(Experiment.from_list(experiment_initialiser))
                    
                elif pat_tuning_started.search(line): # tuning start
                    tuningevent_initialiser = {}
                    date_start = "-".join(str(pat_date.search(line).group()).split(".")[::-1])
                    time_start = str(pat_time.search(line).group())
                    datetime_start = date_start + "T" + time_start
                    
                elif (pat_tuning_finished.search(line) or 
                      pat_no_tuning_necessary.search(line)): # tuning stop
                    tuningevent_initialiser['power_forward'] = float(pat_fwrd.search(line).group())
                    tuningevent_initialiser['power_reflect'] = float(pat_refl.search(line).group())
                    tuningevent_initialiser['position_tune'] = int(pat_tune.search(line).group())
                    tuningevent_initialiser['position_match'] = int(pat_matc.search(line).group())
                    tuningevent_initialiser['SWR'] = float(pat_swr.search(line).group())
                    
                    if pat_no_tuning_necessary.search(line):
                        tuningevent_initialiser['tuning_necessary'] = False
                        date_start = "-".join(str(pat_date.search(line).group()).split(".")[::-1])
                        time_start = str(pat_time.search(line).group())
                        datetime_start = date_start + "T" + time_start
                    else:
                        tuningevent_initialiser['tuning_necessary'] = True
                    
                    tuningevent_initialiser['datetime_start'] = datetime_start
                    date_finish = "-".join(str(pat_date.search(line).group()).split(".")[::-1])
                    time_finish = str(pat_time.search(line).group())
                    datetime_finish = date_finish + "T" + time_finish
                    tuningevent_initialiser['datetime_finish'] = datetime_finish
                    
                    experiment_initialiser.append(TuningEvent.from_dict(tuningevent_initialiser))
                    
        if exp_unfinished: # If the log ends with an experiment that has not been stopped yet
            log_initialiser.append(Experiment.from_list(experiment_initialiser))
                    
        return cls(log_initialiser)
    
    def get_overview_df(self):
        '''
        Returns a pandas dataframe with an overview of the experiments in the log.
        '''
        
        length = []
        datetime_start = []
        datetime_finish = []
        time_hours = []
        for i,exp in enumerate(self):
            length.append(len(exp))
            if len(exp) != 0:
                datetime_start.append(exp[0].datetime_start)
                datetime_finish.append(exp[-1].datetime_finish)
                time_hours.append(np.round((exp[-1].datetime_finish - 
                                       exp[0].datetime_start).total_seconds()/3600,1))
            else:
                datetime_start.append(None)
                datetime_finish.append(None)
                time_hours.append(None)
        
        df_init = {"datetime_start": datetime_start,
                   "datetime_finish": datetime_finish,
                   "time_hours": time_hours,
                   "length": length}
        
        return pd.DataFrame(df_init)

class Experiment(collections.abc.MutableSequence):
    '''
    Sequence of TuningEvents comprising a single, presumably finished experiment.
    All times are in seconds.
    '''
    
    def __init__(self, 
                 events: list):
        self.events = []
        self.events.extend(events)
        
    @property
    def acquisition_time(self):
        '''
        Acquisition time per scan (i.e. number of complex points divided by the
        spectral width). Needs to be set manually by the user.
        '''
        if len(set([e.acquisition_time for e in self])) == 1:
            return self[0].acquisition_time
        else:
            return None
    @acquisition_time.setter
    def acquisition_time(self, acquisition_time: float):
        for e in self:
            e.acquisition_time = acquisition_time
        
    @property
    def frequencies(self):
        frqs = [e.frequency for e in self]
        if all(frqs):
            return frqs
        else:
            return None
    @frequencies.setter
    def frequencies(self, frqs: list):
        if len(frqs) == len(self):
            for i,f in enumerate(frqs):
                self[i].frequency = f
        else:
            raise ValueError("ERROR: frqs is length {0}, should be {1}!".format(
                len(frqs), len(self)))
            
    @property
    def recycle_delay(self):
        if len(set([e.recycle_delay for e in self])) == 1:
            return self[0].recycle_delay
        else:
            return None
    @recycle_delay.setter
    def recycle_delay(self, recycle_delay: float):
        for e in self:
            e.recycle_delay = recycle_delay
            
    @property
    def reflectance(self):
        return [TuningEvent.swr_to_reflectance(e.SWR) for e in self]
            
    @property
    def sample(self):
        if len(set([e.sample for e in self])) == 1:
            return self[0].sample
        else:
            return None
    @sample.setter
    def sample(self, sample: str):
        for e in self:
            e.sample = sample
            
    @property
    def scans_no(self):
        if len(set([e.scans_no for e in self])) == 1:
            return self[0].scans_no
        else:
            return None
    @scans_no.setter
    def scans_no(self, scans_no: int):
        for e in self:
            e.scans_no = scans_no
        
    @property
    def steady_state_no(self):
        if len(set([e.steady_state_no for e in self])) == 1:
            return self[0].steady_state_no
        else:
            return None
    @steady_state_no.setter
    def steady_state_no(self, steady_state_no: int):
        for e in self:
            e.steady_state_no = steady_state_no
            
    @property
    def temperature(self):
        if len(set([e.temperature for e in self])) == 1:
            return self[0].temperature
        else:
            return None
    @temperature.setter
    def temperature(self, temperature: float):
        for e in self:
            e.temperature = temperature
            
    @property
    def timedelta_total(self):
        '''
        The total time between subsequent tuning events as determined by
        the interval between datetime_finish values. The first value,
        lacking a clear starting time, is the average of the rest.
        '''
        timedelta_seconds = []
        for i,e in enumerate(self):
            if i > 0:
                timedelta_seconds.append((self[i].datetime_finish - 
                                    self[i-1].datetime_finish).total_seconds())
        # Pre-append the average
        timedelta_seconds.insert(0, np.mean(timedelta_seconds))
                
        return timedelta_seconds
    
    @property
    def timedelta_tuning(self):
        '''
        Returns a list of the actual tuning time, i.e. the total time between
        datetime_finish of subsequent tuning events *minus* the time spent
        measuring. Measurement time calculated from the recycle delay plus 
        acquisition time (which is per scan), multiplied by the number of
        (dummy) scans. This is still not completely accurate.
        '''
        
        if (self.recycle_delay is None or
            self.steady_state_no is None or
            self.acquisition_time is None):
            return None
        
        time_measurement = (self.scans_no+self.steady_state_no)*(self.recycle_delay+self.acquisition_time) # in seconds
        
        return [t-time_measurement for t in self.timedelta_total]
    
    @property
    def tuning_changed(self):
        '''
        Returns a boolean list of the events where the tuning position has actually
        changed. The 'tuning started' reported in the native atmlog txt is not
        100% reliable, even though the accuracy of a VOCS spectrum relies on the
        tuning *always* changing (because of backlash or something).
        '''
        return ([self[0].tuning_necessary] + 
                [True if self[i].position_tune != self[i-1].position_tune else False for i in range(1,len(self))])
    
    def check(self, v):
        if not isinstance(v, TuningEvent):
            raise TypeError("ERROR: Experiment class only takes TuningEvent instances!")
    
    def __delitem__(self, i): del self.events[i]
    
    def __getitem__(self, i): return self.events[i]
    
    def __len__(self): return len(self.events)
    
    def __setitem__(self, i, v):
        self.check(v)
        self.events[i] = v
    
    def insert(self, i, v):
        self.check(v)
        self.events.insert(i, v)
    
    @classmethod
    def from_list(cls, explist: list):
        '''
        Constructor from a list of TuningEvents.
        '''
        
        return cls(events = explist)
    
    @classmethod
    def from_json(cls, path_json: Path):
        '''
        Constructor from a json file.
        '''
        
        with open(path_json, 'r') as src:
            exp_as_list = json.load(src)
            
        exp_as_list = [TuningEvent.from_dict(e) for e in exp_as_list]
            
        return cls(exp_as_list)
    
    def to_list(self):
        '''
        Returns the experiment as a list of TuningEvent dicts.
        '''
        
        return [tuning.to_dict() for tuning in self]
    
    def to_json(self, path_json: Path) -> bool:
        '''
        Exports the experiment to a json file of the given path.
        '''
        
        if not path_json.parent.is_dir():
            raise IOError("ERROR: {} is not a valid directory!".format(str(path_json.parent)))
        elif path_json.is_file():
            if not {'Y': True, 'N': False}[input(
                    "{} exists! Overwrite (Y/N)? ".format(str(path_json))).format(
                        str(path_json)).upper()]:
                print("Terminating per user request")
                return False
            
        with open(path_json, 'w') as dst:
            json.dump(self.to_list(), dst)
        
        return True
    
    def print_overview(self):
        
        print("eATM log overview")
        if len(self) == 0:
            print("This experiment is empty")
        else:
            print("datetime_start:\t", self[0].datetime_start)
            print("datetime_finish:", self[-1].datetime_finish)
            print("time_hours:\t\t", np.round((self[-1].datetime_finish - 
                                  self[0].datetime_start).total_seconds()/3600,1))
            print("steps:\t\t\t", len(self))
            if np.all(self.frequencies):
                print("frequencies:\t", "present")
            elif np.any(self.frequencies):
                print("frequencies:\t", "partially present")
            else:
                print("frequencies:\t", "absent")
            print("sample:\t\t\t", self.sample)
            print("temperature:\t", self.temperature)
            print("scans_no:\t\t", self.scans_no)
            
    def get_plotargslist(self, 
                         swr_as_refl: bool = False,
                         key_y1 = 'tune', key_y2 = 'match',
                         sort_freq: bool = False,
                         include_kwargs: bool = True):
        '''
        Get readymade input dictionaries for plotting
        the SWR/Reflectance and two other ATM values in a triple y-axis.
        
        Returns plotdicts, a list of three dictionaries:
            x
            y
        and if include_kwargs:
            linestyle
            marker
            linewidth
            markersize
            colour
        '''
        
        plotargslist = [[], [], []]
        
        key_to_attr_event = {'tune': 'position_tune',
                             'match': 'position_match',
                             'fwrd': 'power_forward',
                             'refl': 'power_reflect',
                             'dtime': "time_delta",
                             'tune_started': 'tuning_necessary'}
        
        key_to_attr_exp = {'dtime_true': 'timedelta_tuning',
                           'dtime_total': 'timedelta_total',
                           'tune_changed': 'tuning_changed'}
        
        # X
        if self.frequencies is not None:
            X = np.array([f/1e6 for f in self.frequencies])
        elif self.frequencies is None:
            X = np.array(list(range(len(self))))
            
        # sorting according to frequency (added for coublecomb pattern):
        if sort_freq:
            idx_order = np.argsort(X)
        else:
            idx_order = range(len(X))
            
        for args in plotargslist:
            args.append(X[idx_order])
        
        # Y[0]
        if swr_as_refl:
            plotargslist[0].append(np.array(self.reflectance)[idx_order]*100)
        else:
            plotargslist[0].append(np.array([e.SWR for e in self])[idx_order])
            
        # Y[1], Y[2]
        if key_y1 in key_to_attr_event:
            plotargslist[1].append(np.array([getattr(e,key_to_attr_event[key_y1]) for e in self])[idx_order])
        elif key_y1 in key_to_attr_exp:
            plotargslist[1].append(np.array(getattr(self,key_to_attr_exp[key_y1]))[idx_order])
        if key_y2 in key_to_attr_event:
            plotargslist[2].append(np.array([getattr(e,key_to_attr_event[key_y2]) for e in self])[idx_order])
        elif key_y2 in key_to_attr_exp:
            plotargslist[2].append(np.array(getattr(self,key_to_attr_exp[key_y2]))[idx_order])
            
        # Kwargs
        if include_kwargs:
            plotkwargslist = [{},{},{}]
            linestyle = '-'
            marker = 'o'
            linewidth = 0.25
            markersize = 0.5
            # From crameri batlow reordered:
            colours = [(0.005193,0.098238,0.349842,1.0),
                       (0.992258,0.716210,0.737146,1.0),
                       (0.409455,0.482351,0.241314,1.0)] 
            for i,kwargs in enumerate(plotkwargslist):
                kwargs.update({'linestyle': linestyle,
                               'marker': marker,
                               'linewidth': linewidth,
                               'markersize': markersize,
                               'color': colours[i]})
            return plotargslist, plotkwargslist
        else:
            return plotargslist
    
    def plot_on_ax(self, axs,
                   swr_as_refl: bool = False,
                   key_y1 = 'tune', key_y2 = 'match',
                   spine2_dist: float = 1.15,
                   xlim = None, ylim_swr = None,
                   ylim_1 = None, ylim_2 = None,
                   sort_freq: bool = False,
                   set_grid: bool = True):
        '''
        Plots SWR/Reflectance and two other ATM values in a triple y-axis
        plot on the given axes in list axs.
        '''
    
        ### MISC PREP
        
        # Some renaming for clarity
        ax_swr = axs[0]
        ax_y1 = axs[1]
        ax_y2 = axs[2]
        
        # Figure size is required for some conditional formatting
        figsize = ax_swr.figure.get_size_inches()
        
        # Dictionaries for getting the right ylabels and ylims
        key_to_label = {'tune': 'Tune position (0.6°)',
                        'match': 'Match position  (0.6°)',
                        'fwrd': 'Forward power (dbm)',
                        'refl': 'Reflected power (dbm)',
                        'dtime': "Tuning time (s)",
                        'dtime_true': 'Tuning time actual (s)',
                        'dtime_total': "Time between 'Tuning Finished' (s)",
                        'tune_started': 'Tuning started (1:True)',
                        'tune_changed': 'Tuning changed (1:True)'}
        
        key_to_ylim = {'tune': (6000, 7400),
                        'match': (7500, 10500),
                        'fwrd': (-30, -18),
                        'refl': (None, None),
                        'dtime': (0, 180),
                        'dtime_true': (0,180),
                        'dtime_total': (0,None),
                        'tune_started': (-1.0,3.0),
                        'tune_changed': (-1.5,2.5)}
        
        ### Labels and limits
        if swr_as_refl:
            label_swr = 'Reflectance (%)'
            if ylim_swr is None:  
                ylim_swr = (0.225, 2.755)
        else:
            label_swr = 'Standing Wave Ratio'
            if ylim_swr is None: 
                ylim_swr = (1.1, 1.4)
        if ylim_1 is None:
            ylim_1 = key_to_ylim[key_y1]
        label_1 = key_to_label[key_y1]
        if ylim_2 is None:
            ylim_2 = key_to_ylim[key_y2]
        label_2 = key_to_label[key_y2]
        
        ### Data
        plotargslist, plotkwargslist = self.get_plotargslist(
            swr_as_refl = swr_as_refl,
            key_y1 = key_y1, key_y2 = key_y2,
            sort_freq = sort_freq,
            include_kwargs = True)
        
        ### Y axes
        ax_swr.plot(*plotargslist[0], **plotkwargslist[0])
        ax_swr.set_ylabel(label_swr, color = plotkwargslist[0]['color'])
        ax_swr.tick_params(axis='y', colors= plotkwargslist[0]['color'])
        ax_swr.set_ylim(ylim_swr)
        
        ax_y1.plot(*plotargslist[1], **plotkwargslist[1])
        ax_y1.set_ylabel(label_1, color = plotkwargslist[1]['color'])
        ax_y1.tick_params(axis='y', colors = plotkwargslist[1]['color'])
        ax_y1.set_ylim(ylim_1)
        
        ax_y2.plot(*plotargslist[2], **plotkwargslist[2])
        ax_y2.set_ylabel(label_2, color = plotkwargslist[2]['color'])
        ax_y2.tick_params(axis='y', colors = plotkwargslist[2]['color'])
        ax_y2.set_ylim(ylim_2)
        
        ### X axis (after Y axes because xlim = (None,None) needs a plot)
        if self.frequencies is not None:
            if xlim is None:
                xlim = (min(plotargslist[0][0]), max(plotargslist[0][0]))
            if figsize[0] >= 6:
                ax_swr.xaxis.set_minor_locator(MultipleLocator(0.2))
            if set_grid:
                ax_swr.grid(which = 'major', linewidth = 0.2)
                ax_swr.grid(which = 'minor', linestyle = '--', linewidth = 0.1)
            ax_swr.set_xlabel('RF frequency (MHz)')
        ax_swr.set_xlim(xlim)
        ax_swr.invert_xaxis()
                    
        # Push one spine to the right to make room for two y-labels
        spine2_dist *= (6/4)/(figsize[0]/figsize[1])
        axs[2].spines.right.set_position(('axes', spine2_dist))
        
    def plot(self,
             swr_as_refl: bool = False,
             key_y1 = 'tune', key_y2 = 'match',
             xlim = (None,None), ylim_swr = (None,None),
             ylim_1 = (None,None), ylim_2 = (None,None),
             sort_freq: bool = False,
             set_grid: bool = True,
             figsize: list = None,
             file_out: Path = None):
        '''
        Plots SWR/Reflectance and two other ATM values in a triple y-axis
        plot in a new figure. Saves it to file_out if one is given.
        '''
        
        fig = plt.figure(dpi=300, figsize = figsize)
        
        ax_swr = plt.axes()
        ax_y1 = ax_swr.twinx()
        ax_y2 = ax_swr.twinx()
        
        self.plot_on_ax([ax_swr, ax_y1, ax_y2],
                        swr_as_refl = swr_as_refl,
                        key_y1 = 'tune', key_y2 = 'match',
                        xlim = xlim, ylim_swr = ylim_swr, 
                        ylim_1 = ylim_1, ylim_2 = ylim_2,
                        sort_freq = sort_freq,
                        set_grid = set_grid)
        
        if file_out is not None:
            if file_out.is_file():
                confirmed = {'Y': True, 'N': False}[input(
                    "{} already exists! Overwrite (Y/N)? ".format(str(file_out))).upper()]
                if confirmed:
                    fig.savefig(file_out, dpi=300, bbox_inches = 'tight')
                    print("Saved to {}".format(file_out))
                else:
                    print("Not saving to pdf per user instruction.")
            else:
                fig.savefig(file_out, dpi=300, bbox_inches = 'tight')
                print("Saved to {}".format(file_out))
                
        plt.show()

class TuningEvent:
    '''
    Single event recorded in an ATM logfile. includes events where tuning
    was not necessary or errors occurred.
    '''
    
    _DATETIME_FORMAT = "%Y-%m-%dT%H:%M:%S"
    
    def __init__(self,
                 datetime_start: datetime,
                 datetime_finish: datetime,
                 SWR: float,
                 power_forward: float,
                 power_reflect: float,
                 position_tune: int,
                 position_match: int,
                 tuning_necessary: bool,
                 frequency: float = None,
                 sample: str = None,
                 temperature: float = None,
                 scans_no: int = None,
                 recycle_delay: float = None,
                 steady_state_no: int = None,
                 acquisition_time: float = None):
        self.datetime_start = datetime_start
        self.datetime_finish = datetime_finish
        self.SWR = SWR
        self.power_forward = power_forward
        self.power_reflect = power_reflect
        self.position_tune = position_tune
        self.position_match = position_match
        self.tuning_necessary = tuning_necessary
        self.frequency = frequency
        self.sample = sample
        self.temperature = temperature
        self.scans_no = scans_no
        self.recycle_delay = recycle_delay
        self.steady_state_no = steady_state_no
        self.acquisition_time = acquisition_time
        
    @property
    def time_delta(self):
        '''
        Event tuning time in seconds.
        '''
        return (self.datetime_finish-self.datetime_start).total_seconds()
    
    @property
    def reflectance(self):
        '''
        Fraction of reflectance, as determined by the Standing Wave Ratio SWR.
        '''
        return self.swr_to_reflectance(self.SWR)
    
    @classmethod
    def from_dict(cls, tuningdict: dict):
        '''
        Constructor from a dictionary containing json serialisable versions
        of the arguments for __init__.
        '''
        
        tuningdict['datetime_start'] = datetime.strptime(
            tuningdict['datetime_start'], cls._DATETIME_FORMAT)
        tuningdict['datetime_finish'] = datetime.strptime(
            tuningdict['datetime_finish'], cls._DATETIME_FORMAT)
        
        return cls(**tuningdict)
    
    @classmethod
    def swr_to_reflectance(cls, swr: float):
        '''
        Takes a standing wave ratio (SWR) and returns the fraction of reflectance.
        '''
        
        return ((swr-1)/(swr+1))**2
    
    @classmethod
    def reflectance_to_swr(cls, reflectance: float):
        '''
        Takes the fraction of reflectance and returns the Standing Wave Ratio (SWR).
        '''
        
        return ((1+np.sqrt(reflectance))/(1-np.sqrt(reflectance)))
    
    @classmethod
    def txt_to_datetime(cls, atm_date: str, atm_time: str):
        '''
        Converts the separate date and time strings as they appear in 
        the ATM's log files into a python datetime instance.
        '''
        
        return datetime.strptime(atm_date+"T"+atm_time,
                                 "%d.%m.%YT%H:%M:%S")
    
    def to_dict(self):
        '''
        Returns the TuningEvent as (JSON compatible) dict.
        '''
        
        tuningdict =  {"datetime_start": self.datetime_start.strftime(self._DATETIME_FORMAT),
                       "datetime_finish": self.datetime_finish.strftime(self._DATETIME_FORMAT),
                       "SWR": self.SWR,
                       "power_forward": self.power_forward,
                       "power_reflect": self.power_reflect,
                       "position_tune": self.position_tune,
                       "position_match": self.position_match,
                       "tuning_necessary": self.tuning_necessary,
                       "frequency": self.frequency,
                       "sample": self.sample,
                       "temperature": self.temperature,
                       "scans_no": self.scans_no,
                       "recycle_delay": self.recycle_delay,
                       "steady_state_no": self.steady_state_no,
                       "acquisition_time": self.acquisition_time}
        
        return tuningdict

if __name__ == '__main__':
    
    '''
    TODO:
        Write unit test
    '''
    
    pass