# -*- coding: utf-8 -*-
"""
Created on Mon Mar 20 11:27:13 2023

@author: jopwo
"""

import re
import subprocess
import time
from pathlib import Path
import json
from datetime import datetime, timezone, timedelta

import math
import numpy as np

import powercurve_read

# I_reffrq = 170051601.7 # Hz, vL iodine at 850 MHz
I_REFFRQ = 80025877.8 # Hz, vL ioidine at 400 MHz (new NQR)
USER = 'jwolffs'

def send2Vnmr(command: str) -> bool:
    '''
    Executes arg `command' in VnmrJ.
    '''

    command_full = ['send2Vnmr', '/home/'+USER+'/vnmrsys/.talk', command]
    command_sent = subprocess.run(command_full)
    
    if command_sent.returncode == 0:
        return True
    else:
        return False
    
def read_procpar_times(expno_or_path) -> tuple:
    '''
    Reads the time run and completed of a VnmrJ procpar.
    if `expno_or_path' is an integer n, this is interpreted as expn.
    If `expno_or_path' is a path, it is interpreted as the
    complete path to the *directory* of the corresponding procpar
    '''
    
    if type(expno_or_path) == int:
        path_procpar = Path('/home/'+USER+'/vnmrsys/exp'+str(expno_or_path)) / 'procpar'
    elif expno_or_path.is_dir():
        path_procpar = expno_or_path / 'procpar'
    else:
        raise ValueError("ERROR: expno_or_path must be a valid exp integer OR a valid vnmrj data directory!")
    
    procpar_times = {}
    
    timestart_pattern = re.compile('^time_run ')
    timecomplete_pattern = re.compile('^time_complete ')
    
    with open(path_procpar, 'r') as procpar:
        read_next_line_flag = False
        read_next_line_param = None
        for line in procpar:
            if read_next_line_flag:
                # capture time string and format to datetime
                timestr = line.replace("\n", "").replace('"','')
                timestr = timestr.split(' ')[-1]
                t = date_from_iso_str(timestr)
                
                procpar_times[read_next_line_param] = t
                
                read_next_line_flag = False
                read_next_line_param = None
            else:
                if timestart_pattern.search(line):
                    read_next_line_flag = True
                    read_next_line_param = 'start'
                elif timecomplete_pattern.search(line):
                    read_next_line_flag = True
                    read_next_line_param = 'complete'
                    
    return (procpar_times['start'], procpar_times['complete'])
                    
def date_from_iso_str(iso_str: str, 
                      utc_offset_str = '+0100') -> datetime:
    '''
    Converts the datetime string notation in a VnmrJ procpar file to
    a python datetime.
    '''
    
    dt= datetime.strptime(iso_str+utc_offset_str, '%Y%m%dT%H%M%S%z')
    return dt

def iso_str_from_date(dt: datetime) -> str:
    '''
    Converts a python datetime to the VnmrJ procpar time notation format.
    '''
    
    iso_str = dt.strftime('%Y%m%dT%H%M%S')
    return iso_str

def now() -> datetime:
    '''
    Returns the current, timezone aware datetime.
    '''
    
    return datetime.now(datetime.now().astimezone().tzinfo)

def frequency_doublecomb(frq_min: float, frq_max: float, frq_step: float,
                           frq_jump_min: float = 0.25e6):
    '''
    Generate a list of frequencies across the freq_min:freq_max range, with
    an interval between *sorted* frequencies of freq_step (except at the start
    and the end, where the combs do not overlap) but where
    *list-adjacent* frequencies are at least freq_jump apart.
    In essence, two sorted frequency lists (the 'combs') are created with
    interval 2*freq_step, offset from each other by freq_jump+freq_step. The
    final list is contructed from alternatingly adding from the two combs.
    This means there is a region of width freq_jump at the start and the end
    of the frequency range that is only covered by one comb, and so is half
    as densely sampled.
    '''
    
    frq_jump = frq_step
    while frq_jump < frq_jump_min:
        frq_jump += 2*frq_step
    
    comb1 = np.arange(frq_min, 
                      frq_max-frq_jump+frq_step, 
                      2*frq_step)
    comb2 = np.arange(frq_min+frq_jump+frq_step*2, 
                      frq_max+frq_step,
                      2*frq_step)
    
    # N_total = len(comb1)+len(comb2)
    frequencies = np.array([[i,j] for i,j in zip(comb1,comb2)]).reshape(2*len(comb2))
    if len(comb1) > len(comb2):
        np.append(frequencies,comb1[-1])
    
    return frequencies

def run_exp(expno: int, save_as: Path = None,
            seconds_max: int = 600, seconds_per_delay: int = 2,
            delays_per_message: int = 5,
            verbose: bool = True, ask_overwrite: bool = True):
    '''
    Instructs VnmrJ to run (au) experiment `expno' and wait for it to finish.
    Finishing is determined by the checking for the appearance of a directory
    `save_as' in vnmrsys/data/.
    If no `save_as' is supplied, save/overwrite vnmrsys/data/TEMP.fid.
    Checking happens every `seconds_per_delay' seconds.
    If no directory has appeared after seconds_max, it is presumed something
    has gone wrong and the experiment is forcibly terminated (aa).
    '''
    
    if save_as is None:
        save_as = Path('/home/'+USER+'/vnmrsys/data/TEMP.fid')
        if save_as.exists():
            subprocess.run(['rm', '-r', str(save_as)])
            time.sleep(1) # small delay to wait for deletion
    elif not save_as.parent.is_dir():
        raise IOError("ERROR: {} is not a valid path!".format(save_as))
    
    if save_as.exists():
        if ask_overwrite:
            if not input("Overwrite {} (Y/)? ".format(save_as)).upper() == 'Y':
                print("Terminating update per user instruction.")
                return False
            else:
                subprocess.run(['rm', '-r', str(save_as)])
                time.sleep(1) # small delay to wait for deletion
                
    future_action_save_as =  "wexp(\'svf(\\\'" + str(save_as) + "\\\')\')"
    
    python_start = now()

    send2Vnmr('jexp'+str(expno))
    send2Vnmr(future_action_save_as)
    send2Vnmr('au')
    if verbose:
        print("python start:",python_start.strftime('%H:%M:%S'))
        # print("Started experiment {}".format(expno))
        
    experiment_complete = False
    loop_counter = 0
    while not experiment_complete:
        if loop_counter > (seconds_max/seconds_per_delay):
            send2Vnmr('aa')
            print("Experiment took too long and was aborted!")
            return False

        loop_counter += 1
        if save_as.exists():
            experiment_complete = True
            time.sleep(1) # small delay so it can save properly
            procpar_start_complete = read_procpar_times(save_as)
            if verbose:
                print("Experiment finished:",
                      procpar_start_complete[1].strftime('%H:%M:%S'))
            break
        else:
            time.sleep(seconds_per_delay)
            if verbose and (loop_counter+1) % delays_per_message == 0:
                print("Seconds passed:", (loop_counter+1)*seconds_per_delay)
    
    return True

def run_onepul(acq_expno: int, save_as: Path,
               tof: float = None, nt: int = None, 
               ask_overwrite: bool = True, verbose: bool = False):
    '''
    Run a onepul experiment.
    '''
    
    # I_REFFRQ = 170051601.7 # Hz, vL iodine at 850 MHz
    I_REFFRQ = 80025877.8 # Hz, vL ioidine at 400 MHz (new NQR)
    freq_absolute = tof + I_REFFRQ
    tpwr_exp, aX90_exp = powercurve_read.get_150W(freq_absolute)
    
    if tof:
        send2Vnmr('jexp'+str(acq_expno))
        send2Vnmr('tof = '+str(tof))
        send2Vnmr('tpwr = '+str(tpwr_exp)) # adjust rough power
        send2Vnmr('aX90 = '+str(aX90_exp)) # adjust fine power
        if nt:
            send2Vnmr('nt = '+str(nt)) # adjust number of scans
            
    print("EXP tpwr:",str(tpwr_exp)+" dB")
    print("EXP aX90:",str(aX90_exp))
    return run_exp(acq_expno, save_as = save_as, seconds_max = math.inf,
            ask_overwrite = ask_overwrite, verbose = verbose)

def run_onepul_custom_power(acq_expno: int, save_as: Path,
                            tpwr: int, aX90: float,
                            tof: float = None, nt: int = None,
                            ask_overwrite: bool = True, verbose: bool = False):
    '''
    Run a onepul experiment.
    '''
    
    # I_reffrq = 170051601.7 # Hz, vL iodine at 850 MHz
    # I_REFFRQ = 80025877.8 # Hz, vL ioidine at 400 MHz (new NQR)
    # freq_absolute = tof + I_REFFRQ
    
    if tof:
        send2Vnmr('jexp'+str(acq_expno))
        send2Vnmr('tof = '+str(tof))
        send2Vnmr('tpwr = '+str(tpwr)) # adjust rough power
        send2Vnmr('aX90 = '+str(aX90)) # adjust fine power
        if nt:
            send2Vnmr('nt = '+str(nt)) # adjust number of scans
            
    print("EXP tpwr:",str(tpwr)+" dB")
    print("EXP aX90:",str(aX90))
    return run_exp(acq_expno, save_as = save_as, seconds_max = math.inf,
            ask_overwrite = ask_overwrite, verbose = verbose)

def run_atm(atm_expno: int, tof: float = None, atm_seconds_max: int = 120,
        verbose: bool = True):
    '''
    Performs a pure atm sequence.
    '''
    
    # I_reffrq = 170051601.7 # Hz, vL iodine at 850 MHz
    I_REFFRQ = 80025877.8 # Hz, vL ioidine at 400 MHz (new NQR)
    freq_absolute = tof + I_REFFRQ
    tpwr_atm,aX90_atm = powercurve_read.get_10mW(freq_absolute) # get adjusted power values from table in other script
        
    if tof:
        send2Vnmr('jexp'+str(atm_expno))
        send2Vnmr('tof = '+str(tof)) # tof in Hz(?)
        send2Vnmr('tpwr = '+str(tpwr_atm)) # adjust rough power
        send2Vnmr('aX90 = '+str(aX90_atm)) # adjust fine power
        print("ATM tpwr:",str(tpwr_atm)+" dB")
        print("ATM aX90:",str(aX90_atm))
    return run_exp(atm_expno, 
                   seconds_max = atm_seconds_max, 
                   verbose = verbose)

def run_atm_custom_power(atm_expno: int, tof: float, tpwr: int, aX90: float, 
                         atm_seconds_max: int = 120, verbose: bool = True):
    '''
    Performs a pure atm sequence with specified power settings instead
    of reading a calibration file.
    '''
    
    # I_reffrq = 170051601.7 # Hz, vL iodine at 850 MHz
    I_REFFRQ = 80025877.8 # Hz, vL ioidine at 400 MHz (new NQR)
    freq_absolute = tof + I_REFFRQ
       
    send2Vnmr('jexp'+str(atm_expno))
    send2Vnmr('tof = '+str(tof)) # tof in Hz(?)
    send2Vnmr('tpwr = '+str(tpwr)) # adjust rough power
    send2Vnmr('aX90 = '+str(aX90)) # adjust fine power
    if verbose:
        print("ATM sfrq:",str(freq_absolute)," Hz")
        print("ATM tpwr:",str(tpwr)+" dB")
        print("ATM aX90:",str(aX90))
    return run_exp(atm_expno, 
                   seconds_max = atm_seconds_max, 
                   verbose = verbose)

# def run_swrcheck_interactive(swrcheck_expno: int,
#                              tof: float, tpwr: int, aX90: float,
#                              verbose: bool = False):
#     '''
    
#     '''
    
#     I_reffrq = 170051601.7 # Hz, vL iodine at 850 MHz
#     freq_absolute = tof + I_reffrq
    
#     send2Vnmr('jexp'+str(swrcheck_expno))
#     send2Vnmr('tof = '+str(tof))
#     send2Vnmr('tpwr = '+str(tpwr))
#     send2Vnmr('aX90 = '+str(aX90))
#     send2Vnmr('nt = '+str(1))

#     if verbose:
#         print("SWRcheck sfrq:",str(freq_absolute)," Hz")
#         print("SWRcheck tpwr:",str(tpwr)+" dB")
#         print("SWRcheck aX90:",str(aX90))
    
#     send2Vnmr('aa')
#     input_is_valid = False
#     while not input_is_valid:
#         current_dbm = input("Enter forward dbm: ")
#         try:
#             current_dbm = float(current_dbm)
#         except:
#             print("Yo that in't even a number bruh")
#         else:
#             if -70 < current_dbm < 0:
#                 input_is_valid = True
#             else:
#                 print("Nah mate. You's typin' rubbish.")
#     send2Vnmr('au')
    
#     return [tof, tpwr, aX90, current_dbm]

def atm_iterative_calibration(swrcheck_expno: int, atm_expno: int,
                     tof: float, tpwr: int, aX90: float,
                     target_dbm_range: tuple = (-20.7, -20.3),
                     atm_seconds_max: int = 120,
                     verbose: bool = False, atm_verbose: bool = True):
    
    # I_reffrq = 170051601.7 # Hz, vL iodine at 850 MHz
    I_REFFRQ = 80025877.8 # Hz, vL ioidine at 400 MHz (new NQR)
    freq_absolute = tof + I_REFFRQ

    # Because we need to tune to successive tofs, we need to
    # turn on the automatic tuning in Autotune.
    # Unfortunately this means that the forward power
    # does not update for swrcheck sequences.
    # So we have to use the atm sequence to determine updated
    # forward powers.
    target_dbm = np.mean(target_dbm_range)
    within_range = False
    powerlist = []
    while not within_range:
        if run_atm_custom_power(atm_expno, tof, tpwr, aX90,
                            atm_seconds_max = atm_seconds_max,
                            verbose = atm_verbose):
            input_is_valid = False
            while not input_is_valid:
                current_dbm = input("Enter forward dbm: ")
                try:
                    current_dbm = float(current_dbm)
                except:
                    print("Yo that in't even a number bruh")
                else:
                    if -70 < current_dbm < 0:
                        input_is_valid = True
                    else:
                        print("Nah mate. You's typin' rubbish.")

            powerlist.append([freq_absolute, tpwr, aX90, current_dbm])
            print('')
            if target_dbm_range[0] < current_dbm < target_dbm_range[1]:
                within_range = True
                return powerlist
            else: # Adjust the amplitude if the forward power is wrong!
                aX90 -= 10*int(np.ceil(10*(current_dbm - target_dbm)))

def ATM_power_list(swrcheck_expno: int, atm_expno: int,
                   save_powerlist_path: Path,
                   tofs: list, tpwrs: list, aX90s: list,
                   atm_seconds_max: int = 240, atm_dtof_max: float = 0.5e6,
                   ask_overwrite: bool = True,
                   verbose: bool = True, atm_verbose: bool = True) -> bool:
    '''
    Run an expno for a list of tofs ASSUMING THAT THE EXPNO IS AN ATM 'SWR CHECK' SEQUENCE!
    The expno is preceded by a normal ATM sequence because otherwise the forward power is misleading.
    *Interactively* calibrates the amplitudes needed to get a decent forward power
    through the ATM for every tof.
    Saves the resulting list of power settings and corresponding output to a json.
    '''
    # I_reffrq = 170051601.7 # Hz, vL iodine at 850 MHz
    I_REFFRQ = 80025877.8 # Hz, vL ioidine at 400 MHz (new NQR)

    # A scalar tpwr input is treated as the same tpwr for all tofs
    if type(tpwrs) == int or type(tpwrs) == float:
        tpwrs = len(tofs)*[tpwrs]
    # A scalar aX90 is treated as the aX90 for the first tof only,
    # and each optimised aX90 wil be the starting value for the next tof. 
    if type(aX90s) == int or type(aX90s) == float:
        use_aX90_adaptive = True
        aX90_adaptive = aX90s
    else:
        use_aX90_adaptive = False
    
    # Check that the list for all three settings are equally long
    if len(tofs) != len(tpwrs):
        raise ValueError("ERROR: tofs are length {}, pwrs are length {}!".format(
            len(tofs),len(tpwrs)))
    elif not use_aX90_adaptive and len(tofs) != len(aX90s):
        raise ValueError("ERROR: tofs are length {}, aX90s are length {}!".format(
            len(tofs),len(aX90s)))

    if not save_powerlist_path.parent.is_dir():
        print("Directory '{}' does not exist! Exiting...".format(str(save_powerlist_path.parent)))
        return False
        
    # Overwrite the found power setting / output file?
    if ask_overwrite and save_powerlist_path.is_file():
        confirmed = {'Y': True, 'N': False}[input(
                "{} already exists! Overwrite (Y/N)?: ".format(
                    str(save_powerlist_path))).upper()]
        if not confirmed:
            return False
    
    # Loop over the lists of settings
    try:
        powerlist = []
        for i,tof,tpwr in zip(np.arange(len(tofs)),tofs,tpwrs):

	    # Set the aX90 either with the input or the result of the previous loop
            if use_aX90_adaptive:
                aX90 = aX90_adaptive
            else:
                aX90 = aX90s[i]

            # If the frequency steps are too big the ATM needs to tune intermediate steps
            if i > 0:
                tof_delta = tofs[i]-tofs[i-1]
                tof_tmp = tofs[i-1]
                while tof_delta > atm_dtof_max:
                    tof_tmp += atm_dtof_max
                    tof_delta -= atm_dtof_max
                    print("")
                    print("Dummy tune to sfrq = {}".format(int(tof_tmp+I_REFFRQ)))
                    run_atm_custom_power(atm_expno, tof_tmp, tpwr, aX90,
                                         atm_seconds_max, atm_verbose)
        
            print("")
            print("Starting atm calibration for sfrq = {} Hz, tpwr = {} dB".format(int(tof+I_REFFRQ),tpwr))
            powerlist_i = atm_iterative_calibration(
                    swrcheck_expno = swrcheck_expno, 
                    atm_expno = atm_expno,
                    tof = tof, tpwr = tpwr, aX90 = aX90,
                    atm_seconds_max = atm_seconds_max,
                    verbose = verbose, atm_verbose = atm_verbose)
            if powerlist_i:
                powerlist += powerlist_i
                aX90_adaptive = powerlist_i[-1][2] # set the adaptive aX90 to the one calibrated in this loop
                print("Successfully calibrated at freq = {} Hz, tpwr = {} dB: amp = {}".format(
                                                                           tof+I_REFFRQ,tpwr,aX90_adaptive))
                # if verbose:
                    # print(powerlist_i)
            else:
                break
                print("ERROR: a pulse sequence procedure failed!")
    finally:
        with open(save_powerlist_path, 'w') as dst:
            json.dump(powerlist, dst)
        
    return True

### Experiments should already contain the right pulse sequences!!
swrcheck_expno = 1272 # The exp with the ATM 'SWR check' sequence
atm_expno = 1274 # The exp with the pure ATM sequence

frq_low  = 180.0e6
frq_high = 190.0e6
frq_step = 0.5e6

# Bird can use a normal increasing frequency, no need to doublecomb
frqs = list(np.arange(frq_low, frq_high+frq_step, frq_step))
# frqs = list(reversed(frqs))
tofs = [f-I_REFFRQ+1.8 for f in frqs]
# Add 1.7 Hz to make it a not round number (which can cause issues)
pwrs = 27
aX90s = 2620

today = now().strftime("%y%m%d")
ATM_power_list(swrcheck_expno = swrcheck_expno, atm_expno = atm_expno,
               save_powerlist_path = Path('/home/'+USER+'/vnmrsys/data/Powercurves/powerlist_atm_'+today+'_180_190'),
               tofs = tofs,
               tpwrs = pwrs,
               aX90s = aX90s)
