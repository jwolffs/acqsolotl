# -*- coding: utf-8 -*-
"""
Created on Mon Mar 20 11:27:13 2023

@author: jopwo
"""

import re
import subprocess
import time
from pathlib import Path
from datetime import datetime, timezone, timedelta

import math
import numpy as np

import powercurve_read

# I_reffrq = 170051601.7 # Hz, vL iodine at 850 MHz
I_REFFRQ = 80025877.8 # Hz, vL ioidine at 400 MHz (new NQR)
USER = 'jwolffs'

def send2Vnmr(command: str) -> bool:
    '''
    Executes arg `command' in VnmrJ.
    '''

    command_full = ['send2Vnmr', '/home/'+USER+'/vnmrsys/.talk', command]
    command_sent = subprocess.run(command_full)
    
    if command_sent.returncode == 0:
        return True
    else:
        return False
    
def read_procpar_times(expno_or_path) -> tuple:
    '''
    Reads the time run and completed of a VnmrJ procpar.
    if `expno_or_path' is an integer n, this is interpreted as expn.
    If `expno_or_path' is a path, it is interpreted as the
    complete path to the *directory* of the corresponding procpar
    '''
    
    if type(expno_or_path) == int:
        path_procpar = Path('/home/'+USER+'/vnmrsys/exp'+str(expno_or_path)) / 'procpar'
    elif expno_or_path.is_dir():
        path_procpar = expno_or_path / 'procpar'
    else:
        raise ValueError("ERROR: expno_or_path must be a valid exp integer OR a valid vnmrj data directory!")
    
    procpar_times = {}
    
    timestart_pattern = re.compile('^time_run ')
    timecomplete_pattern = re.compile('^time_complete ')
    
    with open(path_procpar, 'r') as procpar:
        read_next_line_flag = False
        read_next_line_param = None
        for line in procpar:
            if read_next_line_flag:
                # capture time string and format to datetime
                timestr = line.replace("\n", "").replace('"','')
                timestr = timestr.split(' ')[-1]
                t = date_from_iso_str(timestr)
                
                procpar_times[read_next_line_param] = t
                
                read_next_line_flag = False
                read_next_line_param = None
            else:
                if timestart_pattern.search(line):
                    read_next_line_flag = True
                    read_next_line_param = 'start'
                elif timecomplete_pattern.search(line):
                    read_next_line_flag = True
                    read_next_line_param = 'complete'
                    
    return (procpar_times['start'], procpar_times['complete'])
                    
def date_from_iso_str(iso_str: str, 
                      utc_offset_str = '+0100') -> datetime:
    '''
    Converts the datetime string notation in a VnmrJ procpar file to
    a python datetime.
    '''
    
    dt= datetime.strptime(iso_str+utc_offset_str, '%Y%m%dT%H%M%S%z')
    return dt

def iso_str_from_date(dt: datetime) -> str:
    '''
    Converts a python datetime to the VnmrJ procpar time notation format.
    '''
    
    iso_str = dt.strftime('%Y%m%dT%H%M%S')
    return iso_str

def now() -> datetime:
    '''
    Returns the current, timezone aware datetime.
    '''
    
    return datetime.now(datetime.now().astimezone().tzinfo)

def frequency_doublecomb(frq_min: float, frq_max: float, frq_step: float,
                           frq_jump_min: float = 0.25e6):
    '''
    Generate a list of frequencies across the freq_min:freq_max range, with
    an interval between *sorted* frequencies of freq_step (except at the start
    and the end, where the combs do not overlap) but where
    *list-adjacent* frequencies are at least freq_jump apart.
    In essence, two sorted frequency lists (the 'combs') are created with
    interval 2*freq_step, offset from each other by freq_jump+freq_step. The
    final list is contructed from alternatingly adding from the two combs.
    This means there is a region of width freq_jump at the start and the end
    of the frequency range that is only covered by one comb, and so is half
    as densely sampled.
    '''
    
    frq_jump = frq_step
    while frq_jump < frq_jump_min:
        frq_jump += 2*frq_step
    
    comb1 = np.arange(frq_min, 
                      frq_max-frq_jump+frq_step, 
                      2*frq_step)
    comb2 = np.arange(frq_min+frq_jump+frq_step*2, 
                      frq_max+frq_step,
                      2*frq_step)
    
    # N_total = len(comb1)+len(comb2)
    frequencies = np.array([[i,j] for i,j in zip(comb1,comb2)]).reshape(2*len(comb2))
    if len(comb1) > len(comb2):
        np.append(frequencies,comb1[-1])
    
    return frequencies

def run_exp(expno: int, save_as: Path = None,
            seconds_max: int = 600, seconds_per_delay: int = 2,
            delays_per_message: int = 5,
            verbose: bool = True, ask_overwrite: bool = True):
    '''
    Instructs VnmrJ to run (au) experiment `expno' and wait for it to finish.
    Finishing is determined by the checking for the appearance of a directory
    `save_as' in vnmrsys/data/.
    If no `save_as' is supplied, save/overwrite vnmrsys/data/TEMP.fid.
    Checking happens every `seconds_per_delay' seconds.
    If no directory has appeared after seconds_max, it is presumed something
    has gone wrong and the experiment is forcibly terminated (aa).
    '''
    
    if save_as is None:
        save_as = Path('/home/'+USER+'/vnmrsys/data/TEMP.fid')
        if save_as.exists():
            subprocess.run(['rm', '-r', str(save_as)])
            time.sleep(1) # small delay to wait for deletion
    elif not save_as.parent.is_dir():
        raise IOError("ERROR: {} is not a valid path!".format(save_as))
    
    if save_as.exists():
        if ask_overwrite:
            if not input("Overwrite {} (Y/)? ".format(save_as)).upper() == 'Y':
                print("Terminating update per user instruction.")
                return False
            else:
                subprocess.run(['rm', '-r', str(save_as)])
                time.sleep(1) # small delay to wait for deletion
                
    future_action_save_as =  "wexp(\'svf(\\\'" + str(save_as) + "\\\')\')"
    
    python_start = now()

    send2Vnmr('jexp'+str(expno))
    send2Vnmr(future_action_save_as)
    send2Vnmr('au')
    if verbose:
        print("python start:",python_start.strftime('%H:%M:%S'))
        # print("Started experiment {}".format(expno))
        
    experiment_complete = False
    loop_counter = 0
    while not experiment_complete:
        if loop_counter > (seconds_max/seconds_per_delay):
            send2Vnmr('aa')
            print("Experiment took too long and was aborted!")
            return False

        loop_counter += 1
        if save_as.exists():
            experiment_complete = True
            time.sleep(1) # small delay so it can save properly
            procpar_start_complete = read_procpar_times(save_as)
            if verbose:
                print("Experiment finished:",
                      procpar_start_complete[1].strftime('%H:%M:%S'))
            break
        else:
            time.sleep(seconds_per_delay)
            if verbose and (loop_counter+1) % delays_per_message == 0:
                print("Seconds passed:", (loop_counter+1)*seconds_per_delay)
    
    return True

def run_onepul(acq_expno: int, save_as: Path,
               tof: float = None, nt: int = None, 
               ask_overwrite: bool = True, verbose: bool = False):
    '''
    Run a onepul experiment.
    '''
    
    # I_reffrq = 170051601.7 # Hz, vL iodine at 850 MHz
    I_REFFRQ = 80025877.8 # Hz, vL ioidine at 400 MHz (new NQR)
    freq_absolute = tof + I_REFFRQ
    tpwr_exp, aX90_exp = powercurve_read.get_150W(freq_absolute)
    
    if tof:
        send2Vnmr('jexp'+str(acq_expno))
        send2Vnmr('tof = '+str(tof))
        send2Vnmr('tpwr = '+str(tpwr_exp)) # adjust rough power
        send2Vnmr('aX90 = '+str(aX90_exp)) # adjust fine power
        if nt:
            send2Vnmr('nt = '+str(nt)) # adjust number of scans
            
    print("EXP tpwr:",str(tpwr_exp)+" dB")
    print("EXP aX90:",str(aX90_exp))
    return run_exp(acq_expno, save_as = save_as, seconds_max = math.inf,
            ask_overwrite = ask_overwrite, verbose = verbose)

def run_onepul_custom_power(acq_expno: int, save_as: Path,
                            tpwr: int, aX90: float,
                            tof: float = None, nt: int = None,
                            ask_overwrite: bool = True, verbose: bool = False):
    '''
    Run a onepul experiment.
    '''
    
    # I_reffrq = 170051601.7 # Hz, vL iodine at 850 MHz
    I_REFFRQ = 80025877.8 # Hz, vL ioidine at 400 MHz (new NQR)
    freq_absolute = tof + I_REFFRQ
    
    if tof:
        send2Vnmr('jexp'+str(acq_expno))
        send2Vnmr('tof = '+str(tof))
        send2Vnmr('tpwr = '+str(tpwr)) # adjust rough power
        send2Vnmr('aX90 = '+str(aX90)) # adjust fine power
        if nt:
            send2Vnmr('nt = '+str(nt)) # adjust number of scans
            
    print("EXP tpwr:",str(tpwr)+" dB")
    print("EXP aX90:",str(aX90))
    return run_exp(acq_expno, save_as = save_as, seconds_max = math.inf,
            ask_overwrite = ask_overwrite, verbose = verbose)

def bird_list(acq_expno: int, tofs: list, save_as_dir: Path, tpwrs: list, aX90s: list,
              save_as_prefix: str = None, 
              nt: int = None,
              ask_overwrite: bool = True) -> bool:
    '''
    Run an expno for a list of tofs. 
    Note that since there is no atm pulse sequence, this function is only
    suitable for Bird power measurements!
    '''
    # I_reffrq = 170051601.7 # Hz, vL iodine at 850 MHz
    I_REFFRQ = 80025877.8 # Hz, vL ioidine at 400 MHz (new NQR)
    
    if len(tofs) != len(tpwrs):
        raise ValueError("ERROR: tofs are length {}, pwrs are length {}!".format(
            len(tofs),len(tpwrs)))
    elif len(tofs) != len(aX90s):
        raise ValueError("ERROR: tofs are length {}, aX90s are length {}!".format(
            len(tofs),len(aX90s)))

    if save_as_dir is None:
        confirmed = {'Y': True, 'N': False}[input(
                "Nothing will be saved! Are you sure (Y/N)?: ").upper()]
        if not confirmed:
            print("Script aborted per user input")
            return False
    elif save_as_dir.is_dir():
        if ask_overwrite:
            confirmed = {'Y': True, 'N': False}[input(
                    "{} exists! Overwrite (Y/N)?: ".format(str(save_as_dir))).upper()]
        else:
            confirmed = True
        if confirmed:
            subprocess.run(['rm', '-r', str(save_as_dir)])
        else:
            print("Script aborted per user input")
            return False
           
    if save_as_dir is not None:
        mkdir_process = subprocess.run(['mkdir', save_as_dir])
        if mkdir_process.returncode != 0:
            print("returncode:", mkdir_process.returncode)
            return False
    
        if save_as_prefix is None:
            save_as_prefix = ''
    
    print("Pulsing starts in 10 seconds...")
    time.sleep(10) # Wait for the operator to go to the Bird
    for i,tof,tpwr,aX90 in zip(np.arange(len(tofs)),tofs,tpwrs,aX90s):
        if save_as_dir is not None:
            save_as_i = save_as_dir / (str(save_as_prefix)+'frq'+str(int(tof+I_REFFRQ))+'.fid')
        else:
            save_as_i = None
        print("")
        print("Starting acq for sfrq = {}".format(int(tof+I_REFFRQ)))
        if run_onepul_custom_power(acq_expno = acq_expno, tpwr = tpwr, aX90 = aX90,
                                   tof = tof, nt = nt, save_as = save_as_i,
                                   ask_overwrite = ask_overwrite):
            print("Successfully acquired {}".format(save_as_i))
        else:
            break
            print("ERROR: an acq procedure failed!")

    return True

### Experiments should already contain the right pulse sequences!!
bird_30_30_expno = 1270 # The exp with the 30:30 bird sequence

frq_low  = 140.0e6
frq_high = 190.0e6
frq_step = 0.5e6
nt = 2**7

# Bird can use a normal increasing frequency, no need to doublecomb
frqs = list(np.arange(frq_low, frq_high+frq_step, frq_step))
# frqs = list(reversed(frqs))
tofs = [f-I_REFFRQ+1.8 for f in frqs]
 # Add 1.7 Hz to make it a not round number (which can cause issues)
pwrs = len(frqs)*[58]
aX90s = len(frqs)*[4095]

bird_list(acq_expno = bird_30_30_expno,
          save_as_dir = None,
          save_as_prefix = None,
          tofs = tofs,
          tpwrs = pwrs,
          aX90s = aX90s,
          nt = nt)
