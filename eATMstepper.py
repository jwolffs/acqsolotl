# -*- coding: utf-8 -*-
"""
Created on Mon Mar 20 11:27:13 2023

@author: jopwo
"""

import json

import re
import subprocess
import time
from pathlib import Path
from datetime import datetime, timezone, timedelta

import math
import numpy as np

import powercurve_read_1_2 as powercurve_read

I_REFFRQ = 170051601.7 # Hz, vL iodine at 850 MHz
# I_REFFRQ = 80025877.8 # Hz, vL ioidine at 400 MHz
USER = 'jwolffs'

def send2Vnmr(command: str) -> bool:
    '''
    Executes arg `command' in VnmrJ.
    '''

    command_full = ['send2Vnmr', '/home/'+USER+'/vnmrsys/.talk', command]
    command_sent = subprocess.run(command_full)
    
    if command_sent.returncode == 0:
        return True
    else:
        return False
    
def read_procpar_times(expno_or_path) -> tuple:
    '''
    Reads the time run and completed of a VnmrJ procpar.
    if `expno_or_path' is an integer n, this is interpreted as expn.
    If `expno_or_path' is a path, it is interpreted as the
    complete path to the *directory* of the corresponding procpar
    '''
    
    if type(expno_or_path) == int:
        path_procpar = Path('/home/'+USER+'/vnmrsys/exp'+str(expno_or_path)) / 'procpar'
    elif expno_or_path.is_dir():
        path_procpar = expno_or_path / 'procpar'
    else:
        raise ValueError("ERROR: expno_or_path must be a valid exp integer OR a valid vnmrj data directory!")
    
    procpar_times = {}
    
    timestart_pattern = re.compile('^time_run ')
    timecomplete_pattern = re.compile('^time_complete ')
    
    with open(path_procpar, 'r') as procpar:
        read_next_line_flag = False
        read_next_line_param = None
        for line in procpar:
            if read_next_line_flag:
                # capture time string and format to datetime
                timestr = line.replace("\n", "").replace('"','')
                timestr = timestr.split(' ')[-1]
                t = date_from_iso_str(timestr)
                
                procpar_times[read_next_line_param] = t
                
                read_next_line_flag = False
                read_next_line_param = None
            else:
                if timestart_pattern.search(line):
                    read_next_line_flag = True
                    read_next_line_param = 'start'
                elif timecomplete_pattern.search(line):
                    read_next_line_flag = True
                    read_next_line_param = 'complete'
                    
    return (procpar_times['start'], procpar_times['complete'])
                    
def date_from_iso_str(iso_str: str, 
                      utc_offset_str = '+0100') -> datetime:
    '''
    Converts the datetime string notation in a VnmrJ procpar file to
    a python datetime.
    '''
    
    dt= datetime.strptime(iso_str+utc_offset_str, '%Y%m%dT%H%M%S%z')
    return dt

def iso_str_from_date(dt: datetime) -> str:
    '''
    Converts a python datetime to the VnmrJ procpar time notation format.
    '''
    
    iso_str = dt.strftime('%Y%m%dT%H%M%S')
    return iso_str

def now() -> datetime:
    '''
    Returns the current, timezone aware datetime.
    '''
    
    return datetime.now(datetime.now().astimezone().tzinfo)

def frequency_doublecomb(frq_min: float, frq_max: float, frq_step: float,
                           frq_jump_min: float = 0.25e6):
    '''
    Generate a list of frequencies across the freq_min:freq_max range, with
    an interval between *sorted* frequencies of freq_step (except at the start
    and the end, where the combs do not overlap) but where
    *list-adjacent* frequencies are at least freq_jump apart.
    In essence, two sorted frequency lists (the 'combs') are created with
    interval 2*freq_step, offset from each other by freq_jump+freq_step. The
    final list is contructed from alternatingly adding from the two combs.
    This means there is a region of width freq_jump at the start and the end
    of the frequency range that is only covered by one comb, and so is half
    as densely sampled.
    '''
    
    frq_jump = frq_step
    while frq_jump < frq_jump_min:
        frq_jump += 2*frq_step
    
    comb1 = np.arange(frq_min, 
                      frq_max-frq_jump+frq_step, 
                      2*frq_step)
    comb2 = np.arange(frq_min+frq_jump+frq_step*2, 
                      frq_max+frq_step,
                      2*frq_step)
    
    # N_total = len(comb1)+len(comb2)
    frequencies = np.array([[i,j] for i,j in zip(comb1,comb2)]).reshape(2*len(comb2))
    if len(comb1) > len(comb2):
        np.append(frequencies,comb1[-1])
    
    return frequencies

def run_exp(expno: int, save_as: Path = None,
            seconds_max: int = 600, seconds_per_delay: int = 2,
            delays_per_message: int = 5,
            verbose: bool = True, ask_overwrite: bool = True):
    '''
    Instructs VnmrJ to run (au) experiment `expno' and wait for it to finish.
    Finishing is determined by the checking for the appearance of a directory
    `save_as' in vnmrsys/data/.
    If no `save_as' is supplied, save/overwrite vnmrsys/data/TEMP.fid.
    Checking happens every `seconds_per_delay' seconds.
    If no directory has appeared after seconds_max, it is presumed something
    has gone wrong and the experiment is forcibly terminated (aa).
    '''
    
    if save_as is None:
        save_as = Path('/home/'+USER+'/vnmrsys/data/TEMP.fid')
        if save_as.exists():
            subprocess.run(['rm', '-r', str(save_as)])
            time.sleep(1) # small delay to wait for deletion
    elif not save_as.parent.is_dir():
        raise IOError("ERROR: {} is not a valid path!".format(save_as))
    
    if save_as.exists():
        if ask_overwrite:
            if not input("Overwrite {} (Y/)? ".format(save_as)).upper() == 'Y':
                print("Terminating update per user instruction.")
                return False
            else:
                subprocess.run(['rm', '-r', str(save_as)])
                time.sleep(1) # small delay to wait for deletion
                
    future_action_save_as =  "wexp(\'svf(\\\'" + str(save_as) + "\\\')\')"
    
    python_start = now()

    send2Vnmr('jexp'+str(expno))
    send2Vnmr(future_action_save_as)
    send2Vnmr('au')
    if verbose:
        print("python start:",python_start.strftime('%H:%M:%S'))
        # print("Started experiment {}".format(expno))
        
    experiment_complete = False
    loop_counter = 0
    while not experiment_complete:
        if loop_counter > (seconds_max/seconds_per_delay):
            send2Vnmr('aa')
            print("Experiment took too long and was aborted!")
            return False

        loop_counter += 1
        if save_as.exists():
            experiment_complete = True
            time.sleep(2) # small delay so it can save properly
            procpar_start_complete = read_procpar_times(save_as)
            if verbose:
                print("Experiment finished:",
                      procpar_start_complete[1].strftime('%H:%M:%S'))
            break
        else:
            time.sleep(seconds_per_delay)
            if verbose and (loop_counter+1) % delays_per_message == 0:
                print("Seconds passed:", (loop_counter+1)*seconds_per_delay)
    
    return True

def run_atm(atm_expno: int, tof: float = None, atm_seconds_max: int = 120,
        verbose: bool = True):
    '''
    Performs a pure atm sequence.
    '''
    
    # I_REFFRQ = 170051601.7 # Hz, vL iodine at 850 MHz
    # I_REFFRQ = 80025877.8 # Hz, vL ioidine at 400 MHz
    freq_absolute = tof + I_REFFRQ
    tpwr_atm,aX90_atm = powercurve_read.get_m20d5dbm(freq_absolute) # get adjusted power values from table in other script
        
    if tof:
        send2Vnmr('jexp'+str(atm_expno))
        send2Vnmr('tof = '+str(tof)) # tof in Hz(?)
        send2Vnmr('tpwr = '+str(tpwr_atm)) # adjust rough power
        send2Vnmr('aX90 = '+str(aX90_atm)) # adjust fine power
        print("ATM tpwr:",str(tpwr_atm)+" dB")
        print("ATM aX90:",str(aX90_atm))
    return run_exp(atm_expno, 
                   seconds_max = atm_seconds_max, 
                   verbose = verbose)

def run_acq_HE(acq_expno: int, save_as: Path,
               tof: float = None, nt: int = None, 
               ask_overwrite: bool = True, verbose: bool = False):
    '''
    Run a Hahn echo acquisition.
    '''
    
    I_REFFRQ = 170051601.7 # Hz, vL iodine at 850 MHz
    # I_REFFRQ = 80025877.8 # Hz, vL ioidine at 400 MHz
    freq_absolute = tof + I_REFFRQ
    tpwr_exp,aX90_exp = powercurve_read.get_150W(freq_absolute)
    
    if tof:
        send2Vnmr('jexp'+str(acq_expno))
        send2Vnmr('tof = '+str(tof))
        send2Vnmr('tpwr = '+str(tpwr_exp)) # adjust rough power
        send2Vnmr('aX90 = '+str(aX90_exp)) # adjust fine power
        send2Vnmr('aXecho = '+str(aX90_exp)) # adjust fine power echo!!
        if nt:
            send2Vnmr('nt = '+str(nt)) # adjust number of scans
            
    print("EXP tpwr:",str(tpwr_exp)+" dB")
    print("EXP aX90:",str(aX90_exp))
    return run_exp(acq_expno, save_as = save_as, seconds_max = math.inf,
            ask_overwrite = ask_overwrite, verbose = verbose)

def atm_and_acq(acq_expno: int, atm_expno: int, save_as: Path,
                tof: float = None, nt: int = None,
                atm_seconds_max: int = 120,
                ask_overwrite: bool = True, atm_verbose: bool = True):
    '''
    Goes to the ATM experiment to autotune to the requested tof,
    then starts the acquisition experiment and saves the result, along
    with the ATM procpar, to the path in `save_as'.
    `seconds_max_atm' sets an outer limit on the autotune step before
    forced termination.
    '''
    
    # Check validity of save_as output path
    if not save_as.parent.is_dir():
        raise IOError("ERROR: {} is not a valid path!".format(save_as))
    if save_as.exists():
        if ask_overwrite:
            if not input("Overwrite {} (Y/)? ".format(save_as)).upper() == 'Y':
                print("Terminating update per user instruction.")
                return False
            else:
                subprocess.run(['rm', '-r', str(save_as)])
                time.sleep(1) # small delay to wait for deletion
        
    procpar_atm_savename = 'procpar_atm_'+iso_str_from_date(now())
        
    if run_atm(atm_expno, tof,
               atm_seconds_max = atm_seconds_max, 
               verbose = atm_verbose):
        # ATM ran succesfully, now run acquisition:
        if run_acq_HE(acq_expno, save_as, tof, nt, ask_overwrite, verbose = False):
            # move atm procpar to the acquisition directory
            subprocess.run(['mv', 
                            '/home/'+USER+'/vnmrsys/data/TEMP.fid/procpar',
                            save_as / procpar_atm_savename])
            return True
        else:
            return False
    else:
        return False

def atm_and_acq_list(acq_expno: int, atm_expno: int, tofs: list, 
                     save_as_dir: Path, save_as_prefix: str = None, 
                     nt: int = None,
                     atm_seconds_max: int = 240,
                     atm_dtof_max: float = 0.5e6,
                     ask_overwrite: bool = True, atm_verbose: bool = True,
                     save_log: bool = True) -> bool:
    '''
    Autotune and then acquire for a list of tofs. 
    If at any point an autotune procedure takes longer than atm_seconds_max, 
    or an experiment fails in some other way, the whole process stops.
    If the interval between tofs is larger than atm_dtof_max, the ATM will
    maintain a stepsize of atm_dtof_max but no measurement will be done
    until the actual tofs have been reached.
    '''
    I_REFFRQ = 170051601.7 # Hz, vL iodine at 850 MHz
    # I_REFFRQ = 80025877.8 # Hz, vL ioidine at 400 MHz

    if save_as_dir.is_dir():
        if ask_overwrite:
            confirmed = {'Y': True, 'N': False}[input(
                    "{} exists! Overwrite (Y/N)?: ".format(str(save_as_dir))).upper()]
        else:
            confirmed = True
        if confirmed:
            subprocess.run(['rm', '-r', str(save_as_dir)])
        else:
            print("Script aborted per user request")
            return False
            
    mkdir_process = subprocess.run(['mkdir', save_as_dir])
    if mkdir_process.returncode != 0:
        print("returncode:", mkdir_process.returncode)
        return False
    
    if save_as_prefix is None:
        save_as_prefix = ''
    
    log = []
    for i,tof in enumerate(tofs):
        if i > 0:
            tof_delta = tofs[i]-tofs[i-1]
            tof_tmp = tofs[i-1]
            while tof_delta > atm_dtof_max:
                tof_tmp += atm_dtof_max
                tof_delta -= atm_dtof_max
                print("")
                print("Dummy tune to sfrq = {}".format(int(tof_tmp+I_REFFRQ)))
                log.append({'frequency': tof+I_REFFRQ,
                            'acquisition': False,
                            'datetime_start': now()})
                run_atm(atm_expno, tof_tmp, atm_seconds_max, atm_verbose)
        
        save_as_i = save_as_dir / (str(save_as_prefix)+'frq'+str(int(tof+I_REFFRQ))+'.fid')
        print("")
        print("Starting atm_and_acq for sfrq = {}".format(int(tof+I_REFFRQ)))
        log.append({'frequency': tof+I_REFFRQ,
                    'acquisition': True,
                    'datetime_start': now()})
        if atm_and_acq(acq_expno = acq_expno, atm_expno = atm_expno,
                       tof = tof, nt = nt, save_as = save_as_i,
                       atm_seconds_max = atm_seconds_max,
                       ask_overwrite = ask_overwrite, atm_verbose = atm_verbose):
            print("Successfully acquired {}".format(save_as_i))
        else:
            break
            print("ERROR: an atm_and_acq procedure failed!")
            
    if save_log:
        file_log = save_as_dir+'/stepperlog.json'
        with open(file_log, 'w') as dst:
            json.dump(log, dst)

    return True

if __name__ == '__main__':

    '''
    TODO:
        Convert eATMstepper to object oriented structure
        Include currently global variables (USER, I_REFFRQ) as attributes
        Write a unit test 
    '''    

    pass
