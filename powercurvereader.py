# -*- coding: utf-8 -*-
"""
Created on Tue Mar 28 14:23:45 2023

DO NOT USE THIS SCRIPT WITHOUT CHANGING EVERYTHING TO YOUR OWN SITUATION!
IN PARTICULAR, GENERATE YOUR OWN JSON FILE!!!

@author: jopwo
"""

from pathlib import Path
import json

import numpy as np

I_REFFRQ = 170051601.7 # Hz, vL iodine at 850 MHz
# I_REFFRQ = 80025877.8 # Hz, vL ioidine at 400 MHz
# PATH_150W = 

POWERCURVE_DIR = Path("/home/jwolffs/vnmrsys/data/Powercurves")

def _get_amplitude(power: float, power_dB: int, frequency: float, 
                   path_powercurve: Path):
    '''
    Retrieves the amplitude ('fine power') needed to achieve a certain wattage
    for the given rough power (in dB) and frequency in VnmrJ. 
    It fetches these for a precalculated table of frequency: amplitude values 
    in the powercurve json belonging to the specified wattage.
    These tables are obviously specific to the laboratory setup, so
    TAKE CARE THAT FREQUENCY IS ABSOLUTE AND IN Hz!
    
    The powercurves themselves are a product of a PowerProfile object,
    which is defined and explained in a separate file of the same name.
    '''
    
    with open(path_powercurve, 'r') as src:
        powercurve_dict = json.load(src)
        
    frequencies = np.array(powercurve_dict['powercurves'][str(power)][str(power_dB)]['frequencies'])
    amplitudes = np.array(powercurve_dict['powercurves'][str(power)][str(power_dB)]['amplitudes'])
    
    closest_freq_idx = np.argmin(np.abs(frequencies-frequency))
    
    return amplitudes[closest_freq_idx]

def get_150W(frequency: float, power_dB: int = 58):
    '''
    Retrieves the power settings needed to achieve 150 Watts
    for the given frequency in VnmrJ.
    It fetches the amplitude given the power_dB from a precalculated table
    of frequency: amplitude values in the powercurve json.
    These tables are obviously specific to the laboratory setup, so
    TAKE CARE THAT FREQUENCY IS ABSOLUTE AND IN Hz!
    '''
    
    paths = {58: "240403_150W_58dB_extended.json"}
    
    return power_dB, _get_amplitude(150, power_dB, frequency,
                                    POWERCURVE_DIR / paths[power_dB])

def get_m20d5dbm(frequency: float, power_dB: int = 25):
    '''
    Retrieves the power settings needed to achieve -20.5 dbm for the ATM
    for the given frequency in VnmrJ. 
    It fetches the amplitude for the given power_dB from a precalculated 
    table of frequency: amplitude values in the powercurve json.
    These tables are obviously specific to the laboratory setup, so
    TAKE CARE THAT FREQUENCY IS ABSOLUTE AND IN Hz!
    '''

    paths = {25: "240402_m20d5dbm_25dB_extended.json"}
    
    return power_dB, _get_amplitude(-20.5, power_dB, frequency,
                                    POWERCURVE_DIR / paths[power_dB])

if __name__ == '__main__':
    
    frequency = 140.0000017e6
    
    power_dB_150W = 58
    power_dB_m20d5dbm = 25
    power_dB_150W, amplitude_150W = get_150W(frequency = frequency, power_dB = power_dB_150W)
    power_dB_m20d5dbm, amplitude_m20d5dbm = get_m20d5dbm(frequency, power_dB = power_dB_m20d5dbm)
    print("Frequency: {} MHz".format(frequency/1e6,1))
    print("tof: {} Hz".format(np.round(frequency-I_REFFRQ,1)))
    print("150W: tpwr = {0} and aX90 = {1}".format(
            power_dB_150W, amplitude_150W))
    print("-20.5dbm: tpwr = {0} and aX90 = {1}".format(
	    power_dB_m20d5dbm, amplitude_m20d5dbm))

    pass
