# -*- coding: utf-8 -*-
"""
Created on Tue Mar 28 11:31:38 2023

@author: jopwo
"""

from pathlib import Path
import json

import numpy as np
import scipy as sp

import generic_functions as generic

class PowerCurve:
    '''
    Holds the relation between the effective power (as measured by e.g. the Bird)
    and the RF frequency, assuming constant rough and fine power in VnmrJ.
    If the constant rough and fine power are known, it can generate the 
    rough and fine powers necessary to achieve a constant effective power of choice.
    for any frequency within the original data range.
    Effective power is in W, rough power in dB and fine power in relative amplitude.
    '''
    
    def __init__(self, 
                 frequency: np.ndarray,
                 power_out_W: np.ndarray,
                 power_in_rough_dB: float = None,
                 power_in_fine_amp: float = None):
        if len(frequency) != len(power_out_W):
            raise ValueError("ERROR: frequency and power should be of equal length!")
        self.frequency = np.array(frequency)
        self.power_out_W = np.array(power_out_W)
        self.power_in_rough_dB = power_in_rough_dB
        self.power_in_fine_amp = power_in_fine_amp
        if (power_in_rough_dB is not None and
            power_in_fine_amp is not None):
            self.update_references()
        else:
            self.power_reference_W = None
            self.power_reference_W_spline = None
        self.fitted_power_W = None
        self.fitted_frequency = None
        self.fitted_power_rough_dB = None
        self.fitted_power_fine_amp = None
            
    def to_json(self,path_json: Path):
        '''
        Saves the data (both original and fitted) to a .json file.
        '''
        
        powercurve_dict = {'frequency': self.frequency.tolist(),
                           'power_out_W': self.power_out_W.tolist(),
                           'power_in_rough_dB': self.power_in_rough_dB,
                           'power_in_fine_amp': self.power_in_fine_amp,
                           'fitted_power_W': self.fitted_power_W}
        
        for attr in ['power_reference_W',
                     'fitted_frequency', 
                     'fitted_power_rough_dB', 
                     'fitted_power_fine_amp']:
            if getattr(self, attr) is not None:
                powercurve_dict.update({attr: getattr(self, attr).tolist()})
        
        with open(path_json, 'w') as dst:
            json.dump(powercurve_dict, dst)
        
    @classmethod
    def get_reference_power(cls, power_rough_dB: float, power_fine_amp: float,
                            power_out_W: float):
        '''
        Get the reference power for a given combination of rough and fine power
        in VnmrJ and the effective power in Watts.
        The reference power is defined as the power at 0 dB and 4095 amplitude.
        '''
        
        return power_out_W*10**(-power_rough_dB/10)*(4095/power_fine_amp)**2
        
    @classmethod
    def rough_fine_to_watts(cls, power_rough_dB: float, power_fine_amp: float,
                   power_reference_W: float = 0.001) -> float:
        '''
        Converts the VnmrJ notation of power levels 
        (rough power in dB, fine power in amplitude) to Watts.
        Requires a reference power, in Watts, where rough_power_dB = 0
        and fine_power_amp = 4095.
        '''
        return 10**(power_rough_dB/10)*(power_fine_amp/4095)**2*power_reference_W
    
    @classmethod
    def watts_to_rough_fine(cls, power_out_W: float, power_reference_W: float,
                            power_rough_dB_start: float = None,
                            power_fine_amp_min: int = 100):
        '''
        Get the required rough and fine power to achieve the requested
        effective power power_out_W, given the reference power power_reference_W.
        if power_rough_dB_start is provided, this will be used as a starting
        value in the search of a possible combination of rough and fine power.
        power_fine_amp_min sets a minimum value the amplitude is allowed to be.
        '''
        
        if power_rough_dB_start is None:
            power_rough_dB = 0
        else:
            power_rough_dB = power_rough_dB_start
        
        solution_found = False
        n = 0
        while not solution_found and n < 100:
            if cls.rough_fine_to_watts(power_rough_dB,
                                       4095,
                                       power_reference_W) < power_out_W:
                power_rough_dB += 1
                n += 1
            elif cls.rough_fine_to_watts(power_rough_dB,
                                         power_fine_amp_min,
                                         power_reference_W) > power_out_W:
                power_rough_dB -= 1
                n += 1
            else:
                power_fine_amp = 4095*np.sqrt(power_out_W/power_reference_W*10**(-power_rough_dB/10))
                solution_found = True
                
        if solution_found:
            return (power_rough_dB, power_fine_amp)
        else:
            print("WARNING: failed to find appropriate rough and fine powers!")
    
    def update_references(self, spline_degree: int = 2):
        '''
        Recalculates the reference powers per data point.
        '''
        if (self.power_in_rough_dB is None or
            self.power_in_fine_amp is None):
            raise ValueError("ERROR: both rough and fine power need to be not None to calculate reference powers.")
        
        self.power_reference_W = np.array([self.get_reference_power(
            self.power_in_rough_dB,
            self.power_in_fine_amp,
            P) for P in self.power_out_W])
        
        self.power_reference_W_spline = sp.interpolate.make_interp_spline(
            self.frequency, self.power_reference_W, spline_degree)
        
    def get_constant_power_out(self, power_out_W: float,
                               frequency: np.ndarray = None,
                               power_rough_dB_start: float = None):
        '''
        Generate an array of rough and fine powers to get a consistent power
        output in Watts for an array of frequencies. Uses the measured
        frequencies by default.
        '''
        
        if frequency is None:
            frequency = self.frequency
            
        power_rough_dB = []
        power_fine_amp  = []
        for f in frequency:
            prdB_i, prAmp_f = self.watts_to_rough_fine(power_out_W, 
                                                  self.power_reference_W_spline(f),
                                                  power_rough_dB_start)
            power_rough_dB.append(prdB_i)
            power_fine_amp.append(prAmp_f)
            
        self.fitted_power_W = power_out_W
        self.fitted_frequency = frequency
        self.fitted_power_rough_dB = np.array(power_rough_dB)
        self.fitted_power_fine_amp = np.array(power_fine_amp)
            
        return self.fitted_power_rough_dB, self.fitted_power_fine_amp
    
POWERCURVE_150W_230328 = PowerCurve(frequency  = np.arange(159.000e6,181.000e6,1.000e6),
                                     power_out_W    = [175,180,180,174,164,153,146,141,
                                                   136,130,124,117,109,102,
                                                   96,90,87,85,84,82,80,79],
                                     power_in_rough_dB = 52,
                                     power_in_fine_amp = 4050)

POWERCURVE_10mW_230327 = PowerCurve(frequency = np.arange(159.000e6,174.600e6,0.2e6),
                                     power_out_W = [0.0129, 0.0128, 0.0124, 0.0122, 
                                                0.0116, 0.0111, 0.0104, 0.0099, 
                                                0.0091, 0.0086, 0.0079, 0.0074, 
                                                0.0067, 0.0062, 0.0055, 0.0051, 
                                                0.0045, 0.0042, 0.0038, 0.0035, 
                                                0.0032, 0.003, 0.0029, 0.0027, 
                                                0.0028, 0.0027, 0.0028, 0.0028, 
                                                0.0029, 0.003, 0.0031, 0.0034, 
                                                0.0036, 0.0037, 0.0039, 0.0041, 
                                                0.0045, 0.0052, 0.0056, 0.0061, 
                                                0.0068, 0.0069, 0.0072, 0.0079, 
                                                0.0082, 0.0085, 0.0087, 0.009, 
                                                0.0133, 0.0135, 0.0133, 0.0133, 
                                                0.0133, 0.0131, 0.0129, 0.0124, 
                                                0.0117, 0.011, 0.0103, 0.0096, 
                                                0.0088, 0.0081, 0.0074, 0.007, 
                                                0.0064, 0.0059, 0.0054, 0.0045, 
                                                0.0041, 0.0038, 0.0034, 0.0031, 
                                                0.0029, 0.0025, 0.0022, 0.0018, 
                                                0.0014, 0.0011],
                                     power_in_rough_dB = 19,
                                     power_in_fine_amp = 1500)

if __name__ == '__main__':
    
    # POWERCURVE_150W_230328.get_constant_power_out(150,
    #                                               frequency = np.arange(159.0e6, 181.0e6, 0.01e6),
    #                                               power_rough_dB_start = 55)
    # POWERCURVE_150W_230328.to_json(generic._DATA_VNMRJ_ROOT / "POWERCURVE_150W_230328.json")
    
    # POWERCURVE_10mW_230327.get_constant_power_out(0.01,
    #                                               frequency = np.arange(159.0e6, 174.6e6, 0.01e6),
    #                                               power_rough_dB_start = 19)
    # POWERCURVE_10mW_230327.to_json(generic._DATA_VNMRJ_ROOT / "POWERCURVE_10mW_230327.json")
    pass