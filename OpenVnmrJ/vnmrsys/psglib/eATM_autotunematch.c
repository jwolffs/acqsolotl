/*eATM_autotunematch.c - A sequence that starts automatic tuning and matching with
        the NMR Service eATM probe. Controls the eATM with TTL over User
	Channel 1. Sends a continuous wave at tof while tuning. Afterwards,
	performs a dummy acquisition (no pulse).
	NOTE: If the eATM does not finish successfully, this sequence and
	the continuous wave will not end!
	
	Version 1.0.0 2024-03-17
	Jop W. Wolffs                                  */

#include "standard.h"
#include "solidstandard.h"

// Define Values for Phasetables

static int table1[4] = {0,2,1,3};           // phX90
static int table2[4] = {0,2,1,3};           // phRec

#define phX90 t1
#define phRec t2

// ATM TTL sequence value
// TTL_sequence=0 means Autotune only or Tune by List
// TTL_sequence=N>0 means go to #PN (max 4) and Autotune

static int TTL_sequence = 0;

void pulsesequence() {

  //set cw pulse power (should be 0.01W) and turn on
   obsunblank();
   delay(2.0e-6);
   obsoffset(getval("tof"));
   // To account for different RF strengths at different frequencies,
   // The power as set in VnmrJ needs to different for different RF regions
   // This is done with the python script which also starts this pulse sequence
   // The CW power therefore needs to be read from the following
   obspower(tpwr);
   obspwrf(getval("aX90"));
   // obspower(19);
   // obspwrf(1750);
   xmtron();

   // Note that the TTL signal is sent with on/off switches
   // i.e. sp1on();delay(3.0e-3) is equivalent to 3*spl1on();delay(1.0e-3)
   if (TTL_sequence == 0) {
	//trigger Varian to ATM unit AUTOTUNE only or Tune by list
   	delay(1.0e-3);
   	sp1on(); delay(1.0e-3);		//TTL 1
   	sp1off(); delay(1.0e-3);	//TTL 0
   	sp1on(); delay(1.0e-3);		//TTL 1
   	sp1on(); delay(1.0e-3);		//TTL 1
   	sp1on(); delay(1.0e-3);		//TTL 1 
   	sp1off(); delay(1.0e-3);	//TTL 0
	printf("TTL_sequence 0 executed");
   }
   else if (TTL_sequence == 1) {
   	//trigger Varian to ATM unit #P1 and AUTOTUNE 
   	delay(1.0e-3);
   	sp1on(); delay(1.0e-3);         //TTL 1
   	sp1off(); delay(1.0e-3);  	//TTL 0
   	sp1off(); delay(1.0e-3);  	//TTL 0
   	sp1off(); delay(1.0e-3);  	//TTL 0 
   	sp1on(); delay(1.0e-3);	   	//TTL 1 
   	sp1off();  delay(1.0e-3);	//TTL 0
	printf("TTL_sequence 1 executed");
   }
   else if (TTL_sequence == 2) {
	//trigger Varian to ATM unit #P2 and AUTOTUNE 
	delay(1.0e-3);
	sp1on(); delay(1.0e-3);    	//TTL 1
	sp1off(); delay(1.0e-3);	//TTL 0
	sp1on(); delay(1.0e-3);  	//TTL 1 
	sp1off(); delay(1.0e-3); 	//TTL 0
	sp1on(); delay(1.0e-3);		//TTL 1 
	sp1off(); delay(1.0e-3);	//TTL 0
	printf("TTL_sequence 2 executed");
   }
   else if (TTL_sequence == 3) {
	//trigger Varian to ATM unit #P3 and AUTOTUNE 
	delay(1.0e-3);
	sp1on(); delay(1.0e-3);  	//TTL 1
	sp1on(); delay(1.0e-3);  	//TTL 1
	sp1on(); delay(1.0e-3);  	//TTL 1 
	sp1off(); delay(1.0e-3); 	//TTL 0
	sp1on(); delay(1.0e-3);		//TTL 1 
	sp1off(); delay(1.0e-3);	//TTL 0
	printf("TTL_sequence 3 executed");
   }
   else if (TTL_sequence == 4) {
	//trigger Varian to ATM unit #P4 and AUTOTUNE 
	delay(1.0e-3);
	sp1on(); delay(1.0e-3);  	//TTL 1
	sp1on(); delay(1.0e-3);       	//TTL 1 
	sp1off(); delay(1.0e-3);  	//TTL 0
	sp1off(); delay(1.0e-3);      	//TTL 0
	sp1on(); delay(1.0e-3);	   	//TTL 1 
	sp1off(); 			//TTL 0
	printf("TTL_sequence 4 executed");
   }

   //waiting delay
   delay(10.0e-3);

   // Wait for 1 external trigger before continuing
   xgate(1);

   //cw pulse off
   xmtroff();

   // Short delay before switching to the pulse sequence
   hsdelay(1.0);

   // Define Variables and Objects and Get Parameter Values

   double duty;

   DSEQ dec = getdseq("H");

   // Dutycycle Protection

   duty = 4.0e-6 + getval("pwX90") + getval("ad") + getval("rd") + at;
   duty = duty/(duty + d1 + 4.0e-6);
   if (duty > 0.1) {
      printf("Duty cycle %.1f%% >10%%. Abort!\n", duty*100);
      psg_abort(1);
   }

   // Set Phase Tables

   settable(phX90,4,table1);
   settable(phRec,4,table2);
   setreceiver(phRec);

   // Begin Sequence

   txphase(phX90); decphase(zero);
   obspower(tpwr);
   obspwrf(getval("aX90"));
   obsunblank(); decunblank(); _unblank34();
   delay(d1);
   sp1on(); delay(2.0e-6); sp1off(); delay(2.0e-6);

   // X Direct Polarization (not present in this pulsesequence!)

   // Begin Acquisition

   _dseqon(dec);
   obsblank(); _blank34();
   delay(getval("rd"));
   startacq(getval("ad"));
   acquire(np, 1/sw);
   endacq();
   _dseqoff(dec);
   obsunblank(); decunblank(); _unblank34();
}
