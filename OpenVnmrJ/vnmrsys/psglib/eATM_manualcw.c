/*eATM_manualcw.c - A simple continuous wave with some power safeguards
        for the purpose of manually checking the SWR value of the NMR
	Service eATM. No pulse. Dummy acquisition.
	
	Version 1.0.0 2024-06-23
	Jop W. Wolffs                                 */

#include "standard.h"
#include "solidstandard.h"

// Define Values for Phasetables

static int table1[4] = {0,2,1,3};           // phX90
static int table2[4] = {0,2,1,3};           // phRec

#define phX90 t1
#define phRec t2

void pulsesequence() {

   //set cw pulse power (should be ~0.01W) and turn on
   obsunblank();
   delay(2.0e-6);
   obsoffset(getval("tof"));
   // To account for different RF strengths at different frequencies,
   // The power as set in VnmrJ needs to different for different RF regions
   // Unlike the automated ATM sequence, which is controlled through python,
   // the RF power needs to be properly set by the user.
   if (tpwr > 27) {
	printf("twpr is too high! Abort!");
	psg_abort(1);
   }
   obspower(tpwr);
   obspwrf(getval("aX90"));
   
   // Start continuous wave
   xmtron();

   // Arbitrary delay in which to see SWR and possibly tune/match manually
   delay(10.0e0);

   // Stop continuous wave
   xmtroff();

   // Short delay before switching to pulse sequence
   hsdelay(1.0);
   
   // Define Variables and Objects and Get Parameter Values
   
   double duty;

   DSEQ dec = getdseq("H");

   // Dutycycle Protection

   duty = 4.0e-6 + getval("pwX90") + getval("ad") + getval("rd") + at;
   duty = duty/(duty + d1 + 4.0e-6);
   if (duty > 0.1) {
      printf("Duty cycle %.1f%% >10%%. Abort!\n", duty*100);
      psg_abort(1);
   }

   // Set Phase Tables

   settable(phX90,4,table1);
   settable(phRec,4,table2);
   setreceiver(phRec);

   // Begin Sequence

   txphase(phX90); decphase(zero);
   obspower(tpwr);
   obspwrf(getval("aX90"));
   obsunblank(); decunblank(); _unblank34();
   delay(d1);
   sp1on(); delay(2.0e-6); sp1off(); delay(2.0e-6);

   // X Direct Polarization (not present in this pulsesequence!)

   // Begin Acquisition

   _dseqon(dec);
   obsblank(); _blank34();
   delay(getval("rd"));
   startacq(getval("ad"));
   acquire(np, 1/sw);
   endacq();
   _dseqoff(dec);
   obsunblank(); decunblank(); _unblank34();
}
